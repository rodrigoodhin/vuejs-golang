package tokenCache

import (
	"github.com/go-redis/redis"
	"github.com/spf13/viper"
)

// Init - Init data store instance to save users tokens info
func Init() (c *redis.Client, err error) {

	c = redis.NewClient(&redis.Options{
		Addr:     viper.GetString("sessionCache.url"),
		Password: "",
		DB:       0,
	})

	return
}
