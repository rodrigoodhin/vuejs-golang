package db

import (
	"database/sql"
	"log"
	"time"

	"github.com/uptrace/bun/extra/bundebug"

	"github.com/google/uuid"
	"github.com/spf13/viper"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
)

var (
	err   error
	db    *bun.DB
	sqldb *sql.DB
)

func Init() *bun.DB {
	dsn := "postgres://" + viper.GetString("postgres.user") +
		":" + viper.GetString("postgres.pass") +
		"@" + viper.GetString("postgres.host") +
		":" + viper.GetString("postgres.port") +
		"/" + viper.GetString("postgres.db") +
		"?sslmode=disable"
	sqldb = sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))

	db = bun.NewDB(sqldb, pgdialect.New())
	db.AddQueryHook(setLog())

	if err := db.Ping(); err != nil {
		log.Panicf("failed to connect to database: %s", err.Error())
	}

	return db
}

type Model struct {
	ID        uuid.UUID `bun:",pk,type:uuid,default:uuid_generate_v4()"`
	CreatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	DeletedAt time.Time `bun:",soft_delete"`
}

// setLog - Configure db log
func setLog() *bundebug.QueryHook {
	switch viper.GetString("logger.logLevel") {
	case "Silent":
		return bundebug.NewQueryHook(bundebug.WithEnabled(false), bundebug.FromEnv(""))
	case "Info":
		return bundebug.NewQueryHook(bundebug.WithVerbose(true))
	case "Error", "Warn":
		return bundebug.NewQueryHook()
	}

	return nil
}
