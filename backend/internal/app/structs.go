package app

import (
	"github.com/go-redis/redis"
	"github.com/gofiber/fiber/v2"
	"github.com/uptrace/bun"
	rbac "gitlab.com/rodrigoodhin/go-fiber-rbac"
)

type Data struct {
	DB         *bun.DB
	Api        fiber.Router
	Static     fiber.Router
	TokenCache *redis.Client
	RBAC       *rbac.RBAC
}
