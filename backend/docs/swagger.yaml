basePath: /api
definitions:
  fiber.Map:
    additionalProperties: true
    type: object
  handlers.ResponseData:
    properties:
      data:
        $ref: '#/definitions/fiber.Map'
      msg:
        type: string
    type: object
  models.LoginRequest:
    properties:
      browser:
        type: string
      ip:
        type: string
      password:
        type: string
      system:
        type: string
      username:
        type: string
    type: object
  models.PassChangeRequest:
    properties:
      currentPassword:
        type: string
      password:
        type: string
      passwordConfirm:
        type: string
    type: object
  models.PassResetRequest:
    properties:
      browser:
        type: string
      email:
        type: string
      ip:
        type: string
      password:
        type: string
      passwordConfirm:
        type: string
      system:
        type: string
    type: object
  models.UserUpsert:
    properties:
      email:
        type: string
      id:
        type: string
      password:
        type: string
      role:
        items:
          type: string
        type: array
      status:
        type: boolean
      username:
        type: string
    type: object
  rbac.Permission:
    properties:
      action:
        type: string
      id:
        type: string
      model:
        type: string
    type: object
  rbac.Role:
    properties:
      id:
        type: string
      name:
        type: string
    type: object
  rbac.RolePermission:
    properties:
      permission:
        $ref: '#/definitions/rbac.Permission'
      permissionID:
        type: string
      role:
        $ref: '#/definitions/rbac.Role'
      roleID:
        type: string
    type: object
host: localhost:7000
info:
  contact:
    email: contact@backend.io
    name: Backend Support
  description: Swagger for Backend API
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  title: Backend API
  version: "1.0"
paths:
  /auth:
    delete:
      description: deletes user token
      parameters:
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: user logout
      tags:
      - auth
    get:
      description: retrieves user profile data
      parameters:
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: user profile
      tags:
      - auth
    post:
      consumes:
      - application/json
      description: validates login information, creates token and send it in response
      parameters:
      - description: Login Info
        in: body
        name: message
        required: true
        schema:
          $ref: '#/definitions/models.LoginRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "406":
          description: Not Acceptable
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      summary: user login
      tags:
      - auth
    put:
      description: delete old token and creates a new user token
      parameters:
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: refresh token
      tags:
      - auth
  /auth/otp:
    get:
      description: get user OTP key
      parameters:
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: get OTP
      tags:
      - auth
  /auth/otp/:passcode:
    post:
      consumes:
      - application/json
      description: validates OTP login information, creates token and send it in response
      parameters:
      - description: Login Info
        in: body
        name: message
        required: true
        schema:
          $ref: '#/definitions/models.LoginRequest'
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: OTP user login
      tags:
      - auth
    put:
      description: activate user OTP key and return recovery codes
      parameters:
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: activate OTP
      tags:
      - auth
  /role/add/permission:
    post:
      consumes:
      - application/json
      description: adds a permission
      parameters:
      - description: Permission Info
        in: body
        name: message
        required: true
        schema:
          $ref: '#/definitions/rbac.Permission'
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: add permission
      tags:
      - role
  /role/add/role:
    post:
      consumes:
      - application/json
      description: adds a role
      parameters:
      - description: Role Info
        in: body
        name: message
        required: true
        schema:
          $ref: '#/definitions/rbac.Role'
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: add role
      tags:
      - role
  /role/add/rolePermission:
    post:
      consumes:
      - application/json
      description: adds a role permission
      parameters:
      - description: RolePermission Info
        in: body
        name: message
        required: true
        schema:
          $ref: '#/definitions/rbac.RolePermission'
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: add role permission
      tags:
      - role
  /role/delete/permisison/{model}/{action}:
    delete:
      description: deletes a permission
      parameters:
      - description: Permission Model
        in: path
        name: model
        required: true
        type: string
      - description: Permission Action
        in: path
        name: action
        required: true
        type: string
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: delete permission
      tags:
      - role
  /role/delete/role/{name}:
    delete:
      description: deletes a role
      parameters:
      - description: Role Name
        in: path
        name: name
        required: true
        type: string
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: delete role
      tags:
      - role
  /role/delete/rolePermission/{name}/{model}/{action}:
    delete:
      description: deletes a role permission
      parameters:
      - description: Role Name
        in: path
        name: name
        required: true
        type: string
      - description: Permission Model
        in: path
        name: model
        required: true
        type: string
      - description: Permission Action
        in: path
        name: action
        required: true
        type: string
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: delete role permission
      tags:
      - role
  /role/list/all:
    get:
      description: retrieves all roles, permissions and roles permissions
      parameters:
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: list all
      tags:
      - role
  /role/list/permissions:
    get:
      description: retrieves all permissions
      parameters:
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: list permissions
      tags:
      - role
  /role/list/roles:
    get:
      description: retrieves all roles
      parameters:
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: list roles
      tags:
      - role
  /role/list/rolesPermissions:
    get:
      description: retrieves all roles permissions
      parameters:
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: list roles permisisons
      tags:
      - role
  /user:
    get:
      description: retrieves all users
      parameters:
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: list users
      tags:
      - user
    put:
      consumes:
      - application/json
      description: adds or updates a user
      parameters:
      - description: User Info
        in: body
        name: message
        required: true
        schema:
          $ref: '#/definitions/models.UserUpsert'
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: add or update user
      tags:
      - user
  /user/{id}:
    delete:
      description: deletes a user
      parameters:
      - description: User ID
        in: path
        name: id
        required: true
        type: string
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: delete user
      tags:
      - user
    get:
      description: retrieves a user
      parameters:
      - description: User ID
        in: path
        name: id
        required: true
        type: string
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: get user
      tags:
      - user
  /user/password/change/{id}:
    put:
      consumes:
      - application/json
      description: confirm new User password
      parameters:
      - description: User ID
        in: path
        name: id
        required: true
        type: string
      - description: Request Info
        in: body
        name: message
        required: true
        schema:
          $ref: '#/definitions/models.PassChangeRequest'
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: user password change
      tags:
      - user
  /user/password/reset:
    post:
      consumes:
      - application/json
      description: reset user password and send an email with a link to create a new
        one
      parameters:
      - description: Request Info
        in: body
        name: message
        required: true
        schema:
          $ref: '#/definitions/models.PassResetRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      summary: user password reset
      tags:
      - user
    put:
      consumes:
      - application/json
      description: confirm new User password
      parameters:
      - description: Request Info
        in: body
        name: message
        required: true
        schema:
          $ref: '#/definitions/models.PassResetRequest'
      - description: Authorization
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/handlers.ResponseData'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/handlers.ResponseData'
      security:
      - ApiKeyAuth: []
      summary: user password reset confirm
      tags:
      - user
securityDefinitions:
  ApiKeyAuth:
    in: header
    name: ApiKeyAuth
    type: apiKey
swagger: "2.0"
