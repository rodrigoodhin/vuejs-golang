package users

import (
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/raja/argon2pw"
	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/users/models"
	"gorm.io/datatypes"
)

func (d data) migrate() (err error) {

	if _, err = d.DB.NewCreateTable().Model(&models.User{}).IfNotExists().Exec(d.Ctx); err != nil {
		return
	}

	hashedPassword, err := argon2pw.GenerateSaltedHash(viper.GetString("defaultAdminUser.password"))
	if err != nil {
		return
	}

	// Create Default Admin User
	user := &models.User{
		Username: viper.GetString("defaultAdminUser.username"),
		Email:    viper.GetString("defaultAdminUser.email"),
		Password: hashedPassword,
		Status:   true,
		Role:     []string{"admin"},
	}
	_, err = d.DB.NewInsert().
		Model(user).
		On("CONFLICT (username) DO UPDATE").
		On("CONFLICT (email) DO UPDATE").
		Set("password = \"user\".password").
		Set("deleted_at = DEFAULT").
		Exec(d.Ctx)

	return
}

func (d data) loginQuery(username, password string) (user models.User, err error) {
	err = d.DB.NewSelect().
		Column("id", "role", "password", "otp_status").
		Model(&user).
		Where("username = ? and password is not null", username).
		Scan(d.Ctx, &user)
	if err != nil {
		return
	}
	fmt.Println(time.Now().Format("2006-01-02 15:04:05.000000000"))
	valid, err := argon2pw.CompareHashWithPassword(user.Password, password)
	if !valid || err != nil {
		return
	}
	fmt.Println(time.Now().Format("2006-01-02 15:04:05.000000000"))
	return
}

func (d data) pkQuery(userId string) (user models.User, err error) {
	user.ID, err = uuid.Parse(userId)
	if err != nil {
		return
	}

	err = d.DB.NewSelect().
		Model(&user).
		WherePK().
		Scan(d.Ctx, &user)

	return
}

func (d data) setNewOTPKeyQuery(user models.User, otpKey string) (err error) {
	res, err := d.DB.NewUpdate().
		Model(&user).
		WherePK().
		Set("otp_key = ?", otpKey).
		Set("otp_status = ?", false).
		Exec(d.Ctx)
	if err != nil {
		return
	}

	rows, err := res.RowsAffected()
	if rows == 0 || err != nil {
		return fmt.Errorf("record not found")
	}
	return
}

func (d data) activateOTPKeyQuery(user models.User) (err error) {
	res, err := d.DB.NewUpdate().
		Model(&user).
		WherePK().
		Set("otp_status = ?", true).
		Exec(d.Ctx)
	if err != nil {
		return
	}

	rows, err := res.RowsAffected()
	if rows == 0 || err != nil {
		return fmt.Errorf("record not found")
	}

	return
}

func (d data) listUsersQuery() (users []models.UserResponse, err error) {
	err = d.DB.NewSelect().
		Model(&models.User{}).
		Column("id", "username", "email", "status", "role").
		Scan(d.Ctx, &users)

	return
}

func (d data) getUserQuery(userId string) (userResp models.UserResponse, err error) {
	var user models.User

	user.ID, err = uuid.Parse(userId)
	if err != nil {
		return
	}

	err = d.DB.NewSelect().
		ColumnExpr("id, username, email, status, role").
		Model(&user).
		WherePK().
		Scan(d.Ctx, &userResp)

	return
}

func (d data) upsertUserQuery(userUpsert models.UserUpsert) (user models.User, err error) {
	user.Username = userUpsert.Username
	user.Password = userUpsert.Password
	user.Email = userUpsert.Email
	user.Status = userUpsert.Status
	user.Role = userUpsert.Role

	_, err = d.DB.NewInsert().
		Model(&user).
		On("CONFLICT (id) DO UPDATE").
		On("CONFLICT (username) DO UPDATE").
		On("CONFLICT (email) DO UPDATE").
		Set("password = \"user\".password").
		Exec(d.Ctx)

	return
}

func (d data) deleteUserQuery(userId string) (err error) {
	var user models.User

	user.ID, err = uuid.Parse(userId)
	if err != nil {
		return
	}

	res, err := d.DB.NewDelete().
		Model(&user).
		WherePK().
		Exec(d.Ctx)
	if err != nil {
		return
	}

	rows, err := res.RowsAffected()
	if rows == 0 || err != nil {
		return fmt.Errorf("record not found")
	}

	return
}

func (d data) passResetRequestQuery(email string) (user models.User, err error) {
	res, err := d.DB.NewUpdate().
		Model(&user).
		Set("password = ?", *new(string)).
		Set("otp_status = ?", false).
		Set("otp_key = ?", *new(string)).
		Set("otp_recovery_codes = ?", datatypes.JSON{}).
		Where("email = ?", email).
		Exec(d.Ctx)
	if err != nil {
		return
	}

	rows, err := res.RowsAffected()
	if rows == 0 || err != nil {
		return user, fmt.Errorf("record not found")
	}

	return
}

func (d data) passResetConfirmQuery(userId, password string) (err error) {
	res, err := d.DB.NewUpdate().
		Model((*models.User)(nil)).
		Set("password = ?", password).
		Where("id = ? and password is null or password = ?", userId, "").
		Exec(d.Ctx)
	if err != nil {
		return
	}

	rows, err := res.RowsAffected()
	if rows == 0 || err != nil {
		return fmt.Errorf("record not found")
	}

	return
}

func (d data) passChangeQuery(oldPass, newPass, userId string) (err error) {
	var user models.User

	user.ID, err = uuid.Parse(userId)
	if err != nil {
		return
	}

	err = d.DB.NewSelect().
		ColumnExpr("id, password").
		Model(&user).
		WherePK().
		Scan(d.Ctx, &user)
	if user.Password == "" || err != nil {
		return fmt.Errorf("record not found")
	}

	valid, err := argon2pw.CompareHashWithPassword(user.Password, oldPass)
	if !valid || err != nil {
		return fmt.Errorf("invalid current password")
	}

	newPass, err = argon2pw.GenerateSaltedHash(newPass)
	if err != nil {
		return fmt.Errorf("hash generated for new password returned error")
	}

	res, err := d.DB.NewUpdate().
		Model((*models.User)(nil)).
		Set("password = ?", newPass).
		Where("id = ? and password is not null and password <> ?", userId, "").
		Exec(d.Ctx)
	if err != nil {
		return
	}

	rows, err := res.RowsAffected()
	if rows == 0 || err != nil {
		return fmt.Errorf("record not found")
	}

	return
}
