package users

import (
	"log"
	"strings"

	json "github.com/bytedance/sonic"

	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/handlers"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/users/models"
)

// IsRequired - Authentication middleware that returns a new jwt
func (d data) IsRequired() fiber.Handler {
	return jwtware.New(jwtware.Config{
		SigningKey:     []byte(viper.GetString("jwt.secret")),
		ErrorHandler:   d.jwtError,
		SuccessHandler: d.jwtSuccess,
	})
}

// IsRequiredOTP - Authentication middleware for OTP that returns a new jwt
func (d data) IsRequiredOTP() fiber.Handler {
	return jwtware.New(jwtware.Config{
		SigningKey:     []byte(viper.GetString("jwt.otpSecret")),
		ErrorHandler:   d.jwtError,
		SuccessHandler: d.jwtSuccess,
	})
}

// IsRequiredPassReset - Authentication middleware for Password Reset that returns a new jwt
func (d data) IsRequiredPassReset() fiber.Handler {
	return jwtware.New(jwtware.Config{
		SigningKey:     []byte(viper.GetString("jwt.passResetSecret")),
		ErrorHandler:   d.jwtError,
		SuccessHandler: d.jwtSuccess,
	})
}

// login
// @Summary user login
// @Description validates login information, creates token and send it in response
// @Tags auth
// @Accept json
// @Produce json
// @Param message body models.LoginRequest true "Login Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 406 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Router /auth [post]
func (d data) login(c *fiber.Ctx) (err error) {
	loginRequest := new(models.LoginRequest)
	err = c.BodyParser(loginRequest)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Request body is invalid",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	err = loginRequest.Validate()
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusNotAcceptable,
			Msg:    "Invalid login information",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var user models.User

	// validate if user exits in db
	if user, err = d.loginQuery(loginRequest.Username, loginRequest.Password); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid username or password",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var si models.SessionInfo
	si.Browser = loginRequest.Browser
	si.IP = loginRequest.IP
	si.System = loginRequest.System

	if user.OTPStatus {
		// Generate encoded otp token and send it as response.
		t, err := d.GenerateToken(&user, viper.GetString("jwt.otpSecret"), &si)
		if err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Error generating token",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusOK,
			Data:   fiber.Map{"otp": t},
		}.Send()

	} else {
		// Generate encoded token and send it as response.
		t, err := d.GenerateToken(&user, viper.GetString("jwt.secret"), &si)
		if err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Error generating token",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusOK,
			Data:   fiber.Map{"token": t},
		}.Send()
	}
}

// profile
// @Summary user profile
// @Description retrieves user profile data
// @Tags auth
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth [get]
func (d data) profile(c *fiber.Ctx) (err error) {
	var sessions []models.ProfileResponse

	a := strings.Split(d.GetReqTokenRaw(c), ".")
	t := a[1][23:]

	s := d.TokenCache.Keys("*" + t + "*")
	r, err := s.Result()
	if err != nil {
		log.Fatalln(err)
	}

	for _, k := range r {
		session := models.ProfileResponse{}

		session.SessionKey = k

		v := d.TokenCache.Get(k).Val()

		if err := json.Unmarshal([]byte(v), &session.SessionValue); err != nil {
			log.Fatalln(err)
		}

		sessions = append(sessions, session)
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"sessions": sessions},
	}.Send()
}

// refresh
// @Summary refresh token
// @Description delete old token and creates a new user token
// @Tags auth
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth [put]
func (d data) refresh(c *fiber.Ctx) (err error) {
	// Get user token information saved at data store
	tokenCache, err := d.GetTokenCache(c)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var user models.User
	user.ID = tokenCache.UserID
	user.Role = tokenCache.UserRole

	var si models.SessionInfo
	si.Browser = tokenCache.Browser
	si.IP = tokenCache.IP
	si.System = tokenCache.System

	// Generate encoded token
	t, err := d.GenerateToken(&user, viper.GetString("jwt.secret"), &si)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error generating token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Remove old token
	err = d.TokenCache.Del(d.GetReqTokenRaw(c)).Err()
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error deleting old token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"token": t},
	}.Send()
}

// logout
// @Summary user logout
// @Description deletes user token
// @Tags auth
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth [delete]
func (d data) logout(c *fiber.Ctx) (err error) {
	err = d.TokenCache.Del(d.GetReqTokenRaw(c)).Err()
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error deleting token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// otpGet
// @Summary get OTP
// @Description get user OTP key
// @Tags auth
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth/otp [get]
func (d data) otpGet(c *fiber.Ctx) (err error) {
	var user models.User

	// Get user from database
	if user, err = d.pkQuery(d.GetUserId(c)); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Generate a new OTP key
	key := d.GenerateOTP(user.Email)

	// Save user updated to database
	if err = d.setNewOTPKeyQuery(user, key.String()); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to save user OTP key",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Get user token information saved at data store
	tokenCache, err := d.GetTokenCache(c)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var si models.SessionInfo
	si.Browser = tokenCache.Browser
	si.IP = tokenCache.IP
	si.System = tokenCache.System

	// Generate encoded token
	t, err := d.GenerateToken(&user, viper.GetString("jwt.otpSecret"), &si)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error generating token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"qrcode": d.GenerateOTPQRCode(key), "otp": t},
	}.Send()
}

// otpActivate
// @Summary activate OTP
// @Description activate user OTP key and return recovery codes
// @Tags auth
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth/otp/:passcode [put]
func (d data) otpActivate(c *fiber.Ctx) (err error) {
	passcode := c.Params("passcode")

	var user models.User

	// Get user from database
	if user, err = d.pkQuery(d.GetUserId(c)); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Validate the OTP key passcode
	isValid := d.ValidateOTP(user.OTPKey, passcode)
	if !isValid {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to validate OTP key passcode",
		}.Send()
	}

	// Validate that OTOPStatus is not already activated
	if user.OTPStatus {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "OTP key is already activated",
		}.Send()
	}

	// Generate recovery codes
	user.OTPRecoveryCodes = d.GenerateRecoveryCodes()

	// Save user otpStatus updated to database
	if err = d.activateOTPKeyQuery(user); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to activate OTP key at database",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Get user token information saved at data store
	tokenCache, err := d.GetTokenCache(c)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var si models.SessionInfo
	si.Browser = tokenCache.Browser
	si.IP = tokenCache.IP
	si.System = tokenCache.System

	// Generate encoded token and send it as response.
	t, err := d.GenerateToken(&user, viper.GetString("jwt.secret"), &si)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error generating token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"recoveryCodes": user.OTPRecoveryCodes, "token": t},
	}.Send()
}

// otpLogin
// @Summary OTP user login
// @Description validates OTP login information, creates token and send it in response
// @Tags auth
// @Accept json
// @Produce json
// @Param message body models.LoginRequest true "Login Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth/otp/:passcode [post]
func (d data) otpLogin(c *fiber.Ctx) (err error) {
	passcode := c.Params("passcode")

	var user models.User

	// Get user from database
	if user, err = d.pkQuery(d.GetUserId(c)); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Retrieve OTP key from user
	key, err := otp.NewKeyFromURL(user.OTPKey)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error retrieving OTP key",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Validate passcode
	isValid := totp.Validate(passcode, key.Secret())

	if !isValid {
		for _, code := range user.OTPRecoveryCodes {
			if code == passcode {
				isValid = true
			}
		}
	}

	if isValid {
		// Get user token information saved at data store
		tokenCache, err := d.GetTokenCache(c)
		if err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusUnauthorized,
				Msg:    "Invalid token",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

		var si models.SessionInfo
		si.Browser = tokenCache.Browser
		si.IP = tokenCache.IP
		si.System = tokenCache.System

		// Generate encoded token and send it as response.
		t, err := d.GenerateToken(&user, viper.GetString("jwt.secret"), &si)
		if err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Error generating token",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusOK,
			Data:   fiber.Map{"token": t},
		}.Send()

	} else {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Error validating passcode",
		}.Send()
	}
}
