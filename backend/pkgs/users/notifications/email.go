package notifications

import (
	"bytes"
	"embed"
	"fmt"
	"html/template"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/internal/email"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/users/models"
)

//go:embed htmlTmpl/*
var htmlTmpl embed.FS

func SendPasswordResetEmail(username, userEmail, os, browser, token string) (err error) {
	tmpl, err := template.ParseFS(htmlTmpl, "htmlTmpl/password-reset.html")
	if err != nil {
		return fmt.Errorf("error trying to load html embedded : %v", err)
	}

	passwordResetHTML := models.PassResetHTML{
		App:             viper.GetString("app"),
		AppUrl:          viper.GetString("fullUrl"),
		LogoUrl:         viper.GetString("email.logoUrl"),
		Username:        username,
		OperatingSystem: os,
		BrowserName:     browser,
		ActionURL:       viper.GetString("fullUrl") + viper.GetString("email.passResetActionPath") + token,
		CurrentYear:     time.Now().Year(),
	}

	buf := new(bytes.Buffer)
	if err = tmpl.Execute(buf, &passwordResetHTML); err != nil {
		return fmt.Errorf("error trying to parse chart file : %v", err)
	}

	return email.Send("Password Reset", buf.String(), []string{userEmail})
}
