package users

import (
	"database/sql"
	"errors"

	"github.com/gofiber/fiber/v2"
	rbac "gitlab.com/rodrigoodhin/go-fiber-rbac"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/handlers"
)

// listAll
// @Summary list all
// @Description retrieves all roles, permissions and roles permissions
// @Tags role
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /role/list/all [get]
func (d data) listAll(c *fiber.Ctx) (err error) {
	var (
		roles           []rbac.Role
		permissions     []rbac.Permission
		rolePermissions []rbac.RolePermission
	)

	if roles, err = d.RBAC.GetRoles(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Roles",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if permissions, err = d.RBAC.GetPermissions(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Permissions",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if rolePermissions, err = d.RBAC.GetRolePermissions(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Roles Permissions",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"roles": roles, "permissions": permissions, "rolePermissions": rolePermissions},
	}.Send()
}

// listRoles
// @Summary list roles
// @Description retrieves all roles
// @Tags role
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /role/list/roles [get]
func (d data) listRoles(c *fiber.Ctx) (err error) {
	var (
		roles []rbac.Role
	)

	if roles, err = d.RBAC.GetRoles(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Roles",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"roles": roles},
	}.Send()
}

// listPermissions
// @Summary list permissions
// @Description retrieves all permissions
// @Tags role
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /role/list/permissions [get]
func (d data) listPermissions(c *fiber.Ctx) (err error) {
	var (
		permissions []rbac.Permission
	)

	if permissions, err = d.RBAC.GetPermissions(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Permissions",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"permissions": permissions},
	}.Send()
}

// listRolesPermissions
// @Summary list roles permisisons
// @Description retrieves all roles permissions
// @Tags role
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /role/list/rolesPermissions [get]
func (d data) listRolesPermissions(c *fiber.Ctx) (err error) {
	var (
		rolePermissions []rbac.RolePermission
	)

	if rolePermissions, err = d.RBAC.GetRolePermissions(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Roles Permissions",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"rolePermissions": rolePermissions},
	}.Send()
}

// addRole
// @Summary add role
// @Description adds a role
// @Tags role
// @Accept json
// @Produce json
// @Param message body rbac.Role true "Role Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /role/add/role [post]
func (d data) addRole(c *fiber.Ctx) (err error) {
	var (
		role rbac.Role
		res  sql.Result
		rows int64
	)

	if err = c.BodyParser(&role); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	role, res, err = d.RBAC.AddRole(role)

	rows, _ = res.RowsAffected()
	if rows == 0 {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to create Role",
			Data:   fiber.Map{"error": errors.New("Duplicate Role").Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"role": role},
	}.Send()
}

// addPermission
// @Summary add permission
// @Description adds a permission
// @Tags role
// @Accept json
// @Produce json
// @Param message body rbac.Permission true "Permission Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /role/add/permission [post]
func (d data) addPermission(c *fiber.Ctx) (err error) {
	var (
		permission rbac.Permission
		res        sql.Result
		rows       int64
	)

	if err = c.BodyParser(&permission); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	permission, res, err = d.RBAC.AddPermission(permission)

	rows, _ = res.RowsAffected()
	if rows == 0 {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to create Permission",
			Data:   fiber.Map{"error": errors.New("Duplicate Permission").Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"permission": permission},
	}.Send()
}

// addRolePermission
// @Summary add role permission
// @Description adds a role permission
// @Tags role
// @Accept json
// @Produce json
// @Param message body rbac.RolePermission true "RolePermission Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /role/add/rolePermission [post]
func (d data) addRolePermission(c *fiber.Ctx) (err error) {
	var (
		arp            rbac.AddRolePermission
		rolePermission rbac.RolePermission
		res            sql.Result
		rows           int64
	)

	if err = c.BodyParser(&arp); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	rolePermission, res, err = d.RBAC.AddRolePermission(arp)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to create Role Permission",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	rows, _ = res.RowsAffected()
	if rows == 0 {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to create Role Permission",
			Data:   fiber.Map{"error": errors.New("Duplicate Role Permission").Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"rolePermission": rolePermission},
	}.Send()
}

// deleteRole
// @Summary delete role
// @Description deletes a role
// @Tags role
// @Produce json
// @Param name path string true "Role Name"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /role/delete/role/{name} [delete]
func (d data) deleteRole(c *fiber.Ctx) (err error) {
	var (
		res  sql.Result
		rows int64
	)

	res, err = d.RBAC.DeleteRole(c.Params("name"))

	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to delete Role",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	rows, _ = res.RowsAffected()
	if rows == 0 {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to delete Role",
			Data:   fiber.Map{"error": errors.New("Record not found").Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// deletePermission
// @Summary delete permission
// @Description deletes a permission
// @Tags role
// @Produce json
// @Param model path string true "Permission Model"
// @Param action path string true "Permission Action"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /role/delete/permisison/{model}/{action} [delete]
func (d data) deletePermission(c *fiber.Ctx) (err error) {
	var (
		res  sql.Result
		rows int64
	)

	res, err = d.RBAC.DeletePermission(c.Params("model"), c.Params("action"))

	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to delete Permission",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	rows, _ = res.RowsAffected()
	if rows == 0 {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to delete Permission",
			Data:   fiber.Map{"error": errors.New("Record not found").Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// deleteRolePermision
// @Summary delete role permission
// @Description deletes a role permission
// @Tags role
// @Produce json
// @Param name path string true "Role Name"
// @Param model path string true "Permission Model"
// @Param action path string true "Permission Action"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /role/delete/rolePermission/{name}/{model}/{action} [delete]
func (d data) deleteRolePermission(c *fiber.Ctx) (err error) {
	var (
		res  sql.Result
		rows int64
	)

	res, err = d.RBAC.DeleteRolePermission(c.Params("name"), c.Params("model"), c.Params("action"))

	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to delete Role Permission",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	rows, _ = res.RowsAffected()
	if rows == 0 {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to delete Role Permission",
			Data:   fiber.Map{"error": errors.New("Record not found").Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}
