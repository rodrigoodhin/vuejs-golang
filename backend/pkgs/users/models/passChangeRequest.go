package models

import validation "github.com/go-ozzo/ozzo-validation/v4"

// PassChangeRequest -  Password Change Request Data
type PassChangeRequest struct {
	CurrentPassword string `json:"currentPassword"`
	Password        string `json:"password"`
	PasswordConfirm string `json:"passwordConfirm"`
}

func (a PassChangeRequest) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Password, PasswordRule...),
	)
}
