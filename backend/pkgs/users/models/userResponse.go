package models

import (
	"github.com/google/uuid"
)

// UserResponse -  User Response Data
type UserResponse struct {
	ID       uuid.UUID `json:"id"`
	Username string    `json:"username"`
	Email    string    `json:"email"`
	Status   bool      `json:"status"`
	Role     []string  `json:"role" bun:",array"`
}
