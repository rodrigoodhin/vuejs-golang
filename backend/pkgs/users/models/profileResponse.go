package models

// ProfileResponse -  Profile Response Data
type ProfileResponse struct {
	SessionKey   string     `json:"sessionKey"`
	SessionValue TokenCache `json:"sessionValue"`
}
