package models

// PassResetHTML - Password reset email template fields
type PassResetHTML struct {
	App             string
	AppUrl          string
	LogoUrl         string
	Username        string
	OperatingSystem string
	BrowserName     string
	ActionURL       string
	CurrentYear     int
}
