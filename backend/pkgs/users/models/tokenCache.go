package models

import (
	"github.com/google/uuid"
	"time"
)

// TokenCache - Token Cache fields
type TokenCache struct {
	UserID    uuid.UUID `json:"userID"`
	UserRole  []string  `json:"userRole"`
	CreatedAt time.Time `json:"createdAt"`
	ExpireAt  time.Time `json:"expireAt"`
	Browser   string    `json:"browser"`
	IP        string    `json:"ip"`
	System    string    `json:"system"`
}
