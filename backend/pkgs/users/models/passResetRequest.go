package models

import validation "github.com/go-ozzo/ozzo-validation/v4"

// PassResetRequest -  Password Reset Request Data
type PassResetRequest struct {
	Email           string `json:"email"`
	System          string `json:"system"`
	Browser         string `json:"browser"`
	IP              string `json:"ip"`
	Password        string `json:"password"`
	PasswordConfirm string `json:"passwordConfirm"`
}

func (a PassResetRequest) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Password, PasswordRule...),
	)
}
