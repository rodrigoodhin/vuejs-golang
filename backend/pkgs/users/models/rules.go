package models

import (
	"github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
)

var UsernameRule = []validation.Rule{
	validation.Required,
	validation.Length(5, 30),
	is.Alphanumeric,
	validation.Required.Error("Must be a valid Username"),
}

var PasswordRule = []validation.Rule{
	validation.Required,
	validation.Length(8, 20),
	//validation.Match(regexp.MustCompile("^[0-9]{5}$")), //@TODO Add password characters validation
	validation.Required.Error("Must be a valid Password"),
}

var EmailRule = []validation.Rule{
	validation.Required,
	is.Email,
	validation.Required.Error("Must be a valid Email"),
}
