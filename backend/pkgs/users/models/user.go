package models

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/internal/db"
)

// User - User table model
type User struct {
	db.Model
	Username         string   `json:"username" bun:",unique"`
	Password         string   `json:"password"`
	Email            string   `json:"email" bun:",unique"`
	Status           bool     `json:"status"`
	Role             []string `json:"role" bun:",array"`
	OTPKey           string   `json:"otpKey"`
	OTPStatus        bool     `json:"otpStatus"`
	OTPRecoveryCodes []string `json:"otpRecoveryCodes" bun:",array"`
}

func (a User) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Username, UsernameRule...),
		validation.Field(&a.Password, PasswordRule...),
		validation.Field(&a.Email, EmailRule...),
	)
}
