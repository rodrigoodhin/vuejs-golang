package models

// SessionInfo -  Sessin info Data
type SessionInfo struct {
	Browser  string 
	IP       string 
	System   string 
}