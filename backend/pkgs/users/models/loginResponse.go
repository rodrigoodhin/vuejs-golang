package models

// LoginResponse - Login Response Data
type LoginResponse struct {
	Token string `json:"token"`
}
