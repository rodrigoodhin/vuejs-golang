package models

import validation "github.com/go-ozzo/ozzo-validation/v4"

// LoginRequest -  Login Request Data
type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Browser  string `json:"browser"`
	IP       string `json:"ip"`
	System   string `json:"system"`
}

func (a LoginRequest) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Username, UsernameRule...),
		validation.Field(&a.Password, PasswordRule...),
	)
}
