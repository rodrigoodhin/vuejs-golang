package models

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/google/uuid"
)

// UserUpsert - User upsert data
type UserUpsert struct {
	ID       uuid.UUID `json:"id"`
	Username string    `json:"username"`
	Password string    `json:"password"`
	Email    string    `json:"email"`
	Status   bool      `json:"status"`
	Role     []string  `json:"role"`
}

func (a UserUpsert) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Username, UsernameRule...),
		validation.Field(&a.Email, EmailRule...),
	)
}

func (a UserUpsert) ValidatePassword() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Password, PasswordRule...),
	)
}
