package users

import (
	"bytes"
	"encoding/base64"
	"image/png"
	"log"
	"math/rand"
	"time"

	json "github.com/bytedance/sonic"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"github.com/hesahesa/pwdbro"
	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/handlers"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/users/models"
)

// jwtError - Handler error for authentication middleware
func (d data) jwtError(c *fiber.Ctx, err error) error {
	if err.Error() == "Missing or malformed JWT" {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Missing or malformed token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}
	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusUnauthorized,
		Msg:    "Invalid or expired token",
		Data:   fiber.Map{"error": err.Error()},
	}.Send()
}

// jwtSuccess - Handler success for authentication middleware
func (d data) jwtSuccess(c *fiber.Ctx) error {
	// Get user token information saved at data store
	tokenCache, err := d.GetTokenCache(c)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Compare token value with user id
	if tokenCache.UserID.String() != d.GetUserId(c) {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Go to the next middleware
	return c.Next()
}

// GetReqTokenRaw - Get token from request
func (d data) GetReqTokenRaw(c *fiber.Ctx) string {
	return c.Locals("user").(*jwt.Token).Raw
}

// GetClaims - Get claims from request
func (d data) GetClaims(c *fiber.Ctx) (claims jwt.MapClaims) {
	usr := c.Locals("user").(*jwt.Token)
	claims = usr.Claims.(jwt.MapClaims)
	return
}

// CreateResponseUser - Returns a UserResponse object from a User object
func (d data) CreateResponseUser(user models.User) models.UserResponse {
	return models.UserResponse{
		ID:       user.ID,
		Username: user.Username,
		Email:    user.Email,
		Status:   user.Status,
		Role:     user.Role,
	}
}

// GenerateToken - Generate a new token
func (d data) GenerateToken(user *models.User, secret string, si *models.SessionInfo) (t string, err error) {
	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set session timeout
	sessionTimeout := d.GetSessionTimeout()

	// Prepare dates
	timeNow := time.Now()
	expireAt := timeNow.Add(sessionTimeout)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = user.ID
	claims["exp"] = expireAt.Unix()

	// Generate encoded token and send it as response.
	t, err = token.SignedString([]byte(secret))
	if err != nil {
		log.Fatalf("Failed to generate a new token: %v", err)
	}

	// Create token cache instance
	tokenCache := new(models.TokenCache)
	tokenCache.UserID = user.ID
	tokenCache.UserRole = user.Role
	tokenCache.CreatedAt = timeNow
	tokenCache.ExpireAt = expireAt
	tokenCache.Browser = si.Browser
	tokenCache.IP = si.IP
	tokenCache.System = si.System

	// Marshal token cache
	tc, err := json.Marshal(tokenCache)
	if err != nil {
		log.Fatalf("Failed to parse token cache to map: %v", err)
	}

	// Save token in data store
	err = d.TokenCache.Set(t, string(tc), sessionTimeout).Err()
	//err = d.TokenCache.HMSet(t, string(tc), sessionTimeout).Err()
	if err != nil {
		log.Fatalf("Failed to call Put: %v", err)
	}

	return
}

// GetSessionTimeout - Calculate session timeout
func (d data) GetSessionTimeout() time.Duration {
	return viper.GetDuration("jwt.sessionTimeout") * time.Minute
}

// GetTokenCache - Get user token information saved at data store
func (d data) GetTokenCache(c *fiber.Ctx) (tc *models.TokenCache, err error) {
	tokenCacheValue, err := d.TokenCache.Get(d.GetReqTokenRaw(c)).Result()

	if tokenCacheValue != "" {
		err = json.Unmarshal([]byte(tokenCacheValue), &tc)
	}

	return
}

// GenerateOTP - Generate a new OTP key for user
func (d data) GenerateOTP(email string) (key *otp.Key) {
	key, err := totp.Generate(totp.GenerateOpts{
		Issuer:      viper.GetString("url"),
		AccountName: email,
	})
	if err != nil {
		log.Fatalf("Failed to generate a new OTP key: %v", err)
	}

	return
}

// ValidateOTP - Validate the OTP key passcode for user
func (d data) ValidateOTP(key, passcode string) bool {
	otpKey, err := otp.NewKeyFromURL(key)
	if err != nil {
		log.Fatalf("Failed to validate the OTP key passcode: %v", err)
	}

	return totp.Validate(passcode, otpKey.Secret())
}

// GenerateRecoveryCodes - Generate OTP recovery codes
func (d data) GenerateRecoveryCodes() (recoveryCodes []string) {
	letterBytes := viper.GetString("otp.recoveryCodesLetterBytes")

	for k := 0; k < 16; k++ {

		a := make([]byte, 5)
		for i := range a {
			a[i] = letterBytes[rand.Intn(len(letterBytes))]
		}

		b := make([]byte, 5)
		for i := range b {
			b[i] = letterBytes[rand.Intn(len(letterBytes))]
		}

		recoveryCodes = append(recoveryCodes, string(a)+"-"+string(b))
	}

	return
}

// GenerateOTPQRCode - Convert TOTP key into a base64 image
func (d data) GenerateOTPQRCode(key *otp.Key) (imgBase64 string) {
	var buf bytes.Buffer

	img, err := key.Image(200, 200)
	if err != nil {
		log.Fatalf("Failed to generate OTP QR Code: %v", err)
	}

	err = png.Encode(&buf, img)
	if err != nil {
		log.Fatalf("Failed to encode OTP QR Code: %v", err)
	}

	imgBase64 = base64.StdEncoding.EncodeToString(buf.Bytes())

	return
}

// CheckPassStrength - Check user password strength
func (d data) CheckPassStrength(pwd string) (isValid bool, msgs []string, errors []string) {
	status, err := pwdbro.NewDefaultPwdBro().RunParallelChecks(pwd)
	if err != nil {
		log.Fatalf("Failed to validate password: %v", err)
	}

	isValid = true
	for _, resp := range status {
		if !resp.Safe && isValid {
			isValid = resp.Safe
		}

		msgs = append(msgs, resp.Message)

		if resp.Error != nil {
			errors = append(errors, resp.Error.Error())
		}
	}

	return
}

func (d data) GetUserId(c *fiber.Ctx) string {
	return d.GetClaims(c)["id"].(string)
}
