package users

import (
	"context"

	"github.com/go-redis/redis"
	"github.com/uptrace/bun"
	rbac "gitlab.com/rodrigoodhin/go-fiber-rbac"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/internal/app"
)

type data struct {
	DB         *bun.DB
	TokenCache *redis.Client
	RBAC       *rbac.RBAC
	Ctx        context.Context
}

func Init(App *app.Data) (err error) {
	d := &data{
		DB:         App.DB,
		TokenCache: App.TokenCache,
		RBAC:       App.RBAC,
		Ctx:        context.Background(),
	}

	// Auto Migrate
	err = d.migrate()
	if err != nil {
		return
	}

	// Add routers
	d.auth(App.Api.Group("auth"))
	d.user(App.Api.Group("user"))
	d.role(App.Api.Group("role"))

	return nil
}
