package users

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/raja/argon2pw"
	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/handlers"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/users/models"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/users/notifications"
)

// listUsers
// @Summary list users
// @Description retrieves all users
// @Tags user
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user [get]
func (d data) listUsers(c *fiber.Ctx) (err error) {
	var users []models.UserResponse

	if users, err = d.listUsersQuery(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Users",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"users": users},
	}.Send()
}

// getUser
// @Summary get user
// @Description retrieves a user
// @Tags user
// @Produce json
// @Param id path string true "User ID"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/{id} [get]
func (d data) getUser(c *fiber.Ctx) (err error) {
	var user models.UserResponse

	if user, err = d.getUserQuery(c.Params("id")); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"user": user},
	}.Send()
}

// upsertUser
// @Summary add or update user
// @Description adds or updates a user
// @Tags user
// @Accept json
// @Produce json
// @Param message body models.UserUpsert true "User Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user [put]
func (d data) upsertUser(c *fiber.Ctx) (err error) {
	var (
		user       models.User
		userUpsert models.UserUpsert
	)

	if err = c.BodyParser(&userUpsert); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if err = userUpsert.Validate(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to validate User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if userUpsert.Password != "" {

		if err = userUpsert.ValidatePassword(); err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Error to validate User password",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

		if isValid, msgs, errors := d.CheckPassStrength(userUpsert.Password); !isValid {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Error checking password strength",
				Data:   fiber.Map{"msg": strings.Join(msgs, ", "), "error": strings.Join(errors, ", ")},
			}.Send()
		}

		if userUpsert.Password, err = argon2pw.GenerateSaltedHash(userUpsert.Password); err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Hash generated returned error",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

	}

	if user, err = d.upsertUserQuery(userUpsert); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to upsert User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"user": d.CreateResponseUser(user)},
	}.Send()
}

// deleteUser
// @Summary delete user
// @Description deletes a user
// @Tags user
// @Produce json
// @Param id path string true "User ID"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/{id} [delete]
func (d data) deleteUser(c *fiber.Ctx) (err error) {
	if err = d.deleteUserQuery(c.Params("id")); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to delete User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// userPassReset
// @Summary user password reset
// @Description reset user password and send an email with a link to create a new one
// @Tags user
// @Accept json
// @Produce json
// @Param message body models.PassResetRequest true "Request Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Router /user/password/reset [post]
func (d data) userPassReset(c *fiber.Ctx) (err error) {
	var (
		user             models.User
		passResetRequest models.PassResetRequest
	)

	err = c.BodyParser(passResetRequest)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Request body is invalid",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Clear all user login info
	if user, err = d.passResetRequestQuery(passResetRequest.Email); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to clear user login info",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var si models.SessionInfo
	si.Browser = passResetRequest.Browser
	si.IP = passResetRequest.IP
	si.System = passResetRequest.System

	// Generate encoded password reset token
	t, err := d.GenerateToken(&user, viper.GetString("jwt.passResetSecret"), &si)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error generating token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Send password reset email
	err = notifications.SendPasswordResetEmail(user.Username,
		user.Email,
		passResetRequest.System,
		passResetRequest.Browser,
		t)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error sending password reset email",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// userPassResetConfirm
// @Summary user password reset confirm
// @Description confirm new User password
// @Tags user
// @Accept json
// @Produce json
// @Param message body models.PassResetRequest true "Request Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/password/reset [put]
func (d data) userPassResetConfirm(c *fiber.Ctx) (err error) {
	var passResetRequest models.PassResetRequest

	err = c.BodyParser(passResetRequest)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Request body is invalid",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Validate new password
	if err = passResetRequest.Validate(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to validate new password",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Check if password and password confirmation are equals
	if passResetRequest.Password != passResetRequest.PasswordConfirm {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Passwords are different",
		}.Send()
	}

	if passResetRequest.Password, err = argon2pw.GenerateSaltedHash(passResetRequest.Password); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Hash generated returned error",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Save password update to db
	if err = d.passResetConfirmQuery(d.GetUserId(c), passResetRequest.Password); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to update user password",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Delete token
	err = d.TokenCache.Del(d.GetReqTokenRaw(c)).Err()
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error deleting token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// userPassChange
// @Summary user password change
// @Description confirm new User password
// @Tags user
// @Accept json
// @Produce json
// @Param id path string true "User ID"
// @Param message body models.PassChangeRequest true "Request Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/password/change/{id} [put]
func (d data) userPassChange(c *fiber.Ctx) (err error) {
	passChangeRequest := new(models.PassChangeRequest)
	err = c.BodyParser(passChangeRequest)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Request body is invalid",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Validate new password
	if err = passChangeRequest.Validate(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to validate new password",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Check if password and password confirmation are equals
	if passChangeRequest.Password != passChangeRequest.PasswordConfirm {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Passwords are different",
		}.Send()
	}

	// Check password strength
	if isValid, msgs, errors := d.CheckPassStrength(passChangeRequest.Password); !isValid {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error checking password strength",
			Data:   fiber.Map{"msg": strings.Join(msgs, ", "), "error": strings.Join(errors, ", ")},
		}.Send()
	}

	// Check current password and Save password update to db
	if err = d.passChangeQuery(passChangeRequest.CurrentPassword, passChangeRequest.Password, c.Params("id")); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to change user password",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Delete token
	err = d.TokenCache.Del(d.GetReqTokenRaw(c)).Err()
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error deleting token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}
