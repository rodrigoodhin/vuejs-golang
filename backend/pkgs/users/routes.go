package users

import (
	"github.com/gofiber/fiber/v2"
)

func (d data) auth(router fiber.Router) fiber.Router {

	router.Post("/",
		d.login)

	router.Get("/",
		d.IsRequired(),
		d.profile)

	router.Put("/",
		d.IsRequired(),
		d.refresh)

	router.Delete("/",
		d.IsRequired(),
		d.logout)

	router.Get("/otp",
		d.IsRequired(),
		d.otpGet)

	router.Put("/otp/:passcode",
		d.IsRequiredOTP(),
		d.otpActivate)

	router.Post("/otp/:passcode",
		d.IsRequiredOTP(),
		d.otpLogin)

	return router
}

func (d data) user(router fiber.Router) fiber.Router {

	router.Get("/",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "user:list"}),
		d.listUsers)

	router.Get("/:id",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "user:get"}),
		d.getUser)

	router.Put("/",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "user:upsert"}),
		d.upsertUser)

	router.Delete("/:id",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "user:delete"}),
		d.deleteUser)

	router.Post("/password/reset",
		d.userPassReset)

	router.Put("/password/reset",
		d.IsRequiredPassReset(),
		d.RBAC.VerifyRBAC([]string{"admin", "user:passResetConfirm"}),
		d.userPassResetConfirm)

	router.Put("/password/change/:id",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "user:passChange"}),
		d.userPassChange)

	return router
}

func (d data) role(router fiber.Router) fiber.Router {

	router.Get("/list/all",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "role:listAll"}),
		d.listAll)

	router.Get("/list/roles",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "role:listRoles"}),
		d.listRoles)

	router.Get("/list/permissions",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "role:listPermissions"}),
		d.listPermissions)

	router.Get("/list/rolesPermissions",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "role:listRolesPermissions"}),
		d.listRolesPermissions)

	router.Post("/add/role",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "role:addRole"}),
		d.addRole)

	router.Post("/add/permission",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "role:addPermission"}),
		d.addPermission)

	router.Post("/add/rolePermission",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "role:addRolePermission"}),
		d.addRolePermission)

	router.Delete("/delete/role/:name",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "role:deleteRole"}),
		d.deleteRole)

	router.Delete("/delete/permission/:model/:action",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "role:deletePermission"}),
		d.deletePermission)

	router.Delete("/delete/rolePermission/:name/:model/:action",
		d.IsRequired(),
		d.RBAC.VerifyRBAC([]string{"admin", "role:deleteRolePermission"}),
		d.deleteRolePermission)

	return router
}
