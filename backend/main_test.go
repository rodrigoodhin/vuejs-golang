package main

import (
	"bytes"
	"context"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"testing"
	"time"

	json "github.com/bytedance/sonic"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/uptrace/bun/dialect/pgdialect"
	rbac "gitlab.com/rodrigoodhin/go-fiber-rbac"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/internal/db"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/handlers"
	"gitlab.com/rodrigoodhin/vuejs-golang-mysql/backend/pkgs/users/models"
)

const NormalPass = "backendTest"
const HashPass = "argon2$4$32768$4$32$cjwSdQCnJMJ8PtmrKaTXtQ==$gXdGnnOEzwJ49qbT9acutALLWJk9DRksuHeMUU/G86c="

var RecoveryCodes = []string{"5ld36-sl6mr", "hh0x5-ooqi9", "m84xf-1h97i", "d8lf7-wh836", "on9k4-tfv64", "k5yy7-z806c", "e7cat-g9djm", "u0cnb-5be45", "p6a91-i5yet", "999lx-ajvbg", "ej0e3-x7xvk", "g09vy-h0rij", "91w5f-uh950", "ljd0w-462qm", "5q18a-7tf36", "p9941-f4sxu"}

var ctx = context.Background()

// APITest - Struct with all necessarily information for tests
type APITest struct {
	description       string
	method            string
	route             string
	requestBody       map[string]interface{}
	expectedCode      int
	expectedBodyMsg   string
	expectedBodyTexts []string
	//expectedBodyData  fiber.Map
	token string
}

// userTest - User that is used in tests
var userTest models.User

// service - Fiber app service
var service *fiber.App

// TestMain - Creates data that will be used in all tests
func TestMain(m *testing.M) {
	// Setup the app as it is done in the main function
	service = Setup()

	// Disable output
	log.SetOutput(ioutil.Discard)
	os.Exit(m.Run())
}

// TestIndexRoute - Tests for Index route
// 1 - index route
// 2 - non existing route
func TestIndexRoute(t *testing.T) {
	// Define a structure for specifying input and output
	// data of a single test case. This structure is then used
	// to create a so called test map, which contains all test
	// cases, that should be run for testing this function
	tests := []APITest{
		{
			description:       "index route",
			method:            "GET",
			route:             "/",
			expectedCode:      200,
			expectedBodyTexts: []string{"Backend "},
		},
		{
			description:       "non existing route",
			method:            "GET",
			route:             "/i-dont-exist",
			expectedCode:      404,
			expectedBodyTexts: []string{"Cannot GET /i-dont-exist"},
		},
	}

	// Iterate through test single test cases
	for _, test := range tests {

		res, body, err := doRequest(test)

		// Reading the response body should work everytime, such that
		// the "err" variable should be nil
		assert.Nilf(t, err, test.description)

		// Verify if the status code is as expected
		assert.Equalf(t, test.expectedCode, res.StatusCode, test.description)

		// Verify, that the response body equals the expected body
		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, test.description)
		}
	}
}

// TestAuthLoginRoute - Tests for auth login route
// 1 - successfully login
// 2 - empty body
// 3 - invalid username
// 4 - empty username
// 5 - invalid password
// 6 - empty password
// 7 - body with wrong field name
func TestAuthLoginRoute(t *testing.T) {
	initUser("AuthLogin", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description: "successfully login",
			method:      "POST",
			route:       "/api/auth",
			requestBody: map[string]interface{}{
				"username": userTest.Username,
				"password": NormalPass,
			},
			expectedCode: 200,
		},
		{
			description:     "empty body",
			method:          "POST",
			route:           "/api/auth",
			requestBody:     nil,
			expectedCode:    400,
			expectedBodyMsg: "Request body is invalid",
		},
		{
			description: "invalid username",
			method:      "POST",
			route:       "/api/auth",
			requestBody: map[string]interface{}{
				"username": "testTest",
				"password": NormalPass,
			},
			expectedCode:    401,
			expectedBodyMsg: "Invalid username or password",
		},
		{
			description: "empty username",
			method:      "POST",
			route:       "/api/auth",
			requestBody: map[string]interface{}{
				"username": "",
				"password": NormalPass,
			},
			expectedCode:    406,
			expectedBodyMsg: "Invalid login information",
		},
		{
			description: "invalid password",
			method:      "POST",
			route:       "/api/auth",
			requestBody: map[string]interface{}{
				"username": userTest.Username,
				"password": "testTest",
			},
			expectedCode:    401,
			expectedBodyMsg: "Invalid username or password",
		},
		{
			description: "empty password",
			method:      "POST",
			route:       "/api/auth",
			requestBody: map[string]interface{}{
				"username": userTest.Username,
				"password": "",
			},
			expectedCode:    406,
			expectedBodyMsg: "Invalid login information",
		},
		{
			description: "body with wrong field name",
			method:      "POST",
			route:       "/api/auth",
			requestBody: map[string]interface{}{
				"user": userTest.Username,
				"pass": NormalPass,
			},
			expectedCode:    406,
			expectedBodyMsg: "Invalid login information",
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		respBody := readBodyMsg(body)

		assert.Nil(t, err, "Error sending request | "+test.description)
		assert.Equal(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		if test.expectedCode == 200 {
			token := respBody.Data["token"].(string)
			assert.Len(t, token, 164, "Token length is different from expected | "+test.description)
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
			assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)
		}

	}

	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).ForceDelete().Exec(ctx)
}

// TestAuthProfileRoute - Tests for auth profile route
// 1 - profile route
// 2 - invalid token
// 3 - empty token
// 4 - OTP token not allowed
func TestAuthProfileRoute(t *testing.T) {
	initUser("AuthProfile", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description:  "profile route",
			method:       "GET",
			route:        "/api/auth",
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"data":`,
				`"sessions":`,
				`"sessionKey":`,
				`"sessionValue":`,
				`"userID":`,
				`"userRole":`,
				`"createdAt":`,
				`"expireAt":`,
				`"browser":`,
				`"ip":`,
				`"system":`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "GET",
			route:           "/api/auth",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "GET",
			route:           "/api/auth",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "GET",
			route:           "/api/auth",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, test.description)
		}
	}

	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).ForceDelete().Exec(ctx)
}

// TestAuthRefreshRoute - Tests for auth refresh route
// 1 - token successfully refreshed
// 2 - invalid token
// 3 - empty token
// 4 - OTP token not allowed
func TestAuthRefreshRoute(t *testing.T) {
	initUser("AuthRefresh", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description:  "token successfully refreshed",
			method:       "PUT",
			route:        "/api/auth",
			expectedCode: 200,
			token:        getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "PUT",
			route:           "/api/auth",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "PUT",
			route:           "/api/auth",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "PUT",
			route:           "/api/auth",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {
		time.Sleep(1000 * time.Millisecond)

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		if test.expectedCode == 200 {
			newToken := respBody.Data["token"].(string)
			assert.Len(t, test.token, 164, "Token length is different from expected | "+test.description)
			assert.NotEqual(t, test.token, newToken, "New token is not different from the last token | "+test.description)
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).ForceDelete().Exec(ctx)
}

// TestAuthLogoutRoute - Tests for auth logout route
// 1 - successfully logout
// 2 - invalid token
// 3 - empty token
// 4 - OTP token not allowed
func TestAuthLogoutRoute(t *testing.T) {
	initUser("AuthLogout", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description:  "successfully logout",
			method:       "DELETE",
			route:        "/api/auth",
			expectedCode: 200,
			token:        getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "DELETE",
			route:           "/api/auth",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "DELETE",
			route:           "/api/auth",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "DELETE",
			route:           "/api/auth",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).ForceDelete().Exec(ctx)
}

// TestAuthOTPGetRoute - Tests for auth OTP get route
// 1 - OTP key successfully generated
// 2 - invalid token
// 3 - empty token
// 4 - OTP token not allowed
func TestAuthOTPGetRoute(t *testing.T) {
	initUser("AuthOTPGet", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description:  "OTP key successfully generated",
			method:       "GET",
			route:        "/api/auth/otp",
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"qrcode":`,
				`"otp":`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "GET",
			route:           "/api/auth/otp",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "GET",
			route:           "/api/auth/otp",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "GET",
			route:           "/api/auth/otp",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, test.description)
		}

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).ForceDelete().Exec(ctx)
}

// TestAuthOTPActivateRoute - Tests for auth OTP activate route
// 1 - invalid passcode
// 2 - invalid token
// 3 - empty token
// 4 - normal token not allowed
func TestAuthOTPActivateRoute(t *testing.T) {
	initUser("AuthOTPActivate", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description:     "invalid passcode",
			method:          "PUT",
			route:           "/api/auth/otp/878787",
			expectedCode:    500,
			expectedBodyMsg: "Error to validate OTP key passcode",
			token:           getUserToken(service, "OTP"),
		},
		{
			description:     "invalid token",
			method:          "PUT",
			route:           "/api/auth/otp/878787",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "PUT",
			route:           "/api/auth/otp/878787",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "normal token not allowed",
			method:          "PUT",
			route:           "/api/auth/otp/878787",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "NORMAL"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, test.description)
		}
	}

	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).ForceDelete().Exec(ctx)
}

// TestAuthOTPLoginRoute - Tests for auth OTP login route
// 1 - invalid passcode
// 2 - invalid token
// 3 - empty token
// 4 - normal token not allowed
// 5 - user successfully login using a recovery code
func TestAuthOTPLoginRoute(t *testing.T) {
	initUser("AuthOTPLogin", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description:     "invalid passcode",
			method:          "POST",
			route:           "/api/auth/otp/878787",
			expectedCode:    401,
			expectedBodyMsg: "Error validating passcode",
			token:           getUserToken(service, "OTP"),
		},
		{
			description:     "invalid token",
			method:          "POST",
			route:           "/api/auth/otp/878787",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "POST",
			route:           "/api/auth/otp/878787",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "normal token not allowed",
			method:          "POST",
			route:           "/api/auth/otp/878787",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "NORMAL"),
		},
		{
			description:  "user successfully login using a recovery code",
			method:       "POST",
			route:        "/api/auth/otp/" + RecoveryCodes[0],
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"token":`,
			},
			token: getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, test.description)
		}

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username = ?", userTest.Username).ForceDelete().Exec(ctx)
}

// TestUserUpsertRoute - Tests for user upsert route
// 1 - user successfully added
// 2 - invalid username length
// 3 - invalid password length
// 4 - invalid email
// 5 - missing status and role
// 6 - invalid token
// 7 - empty token
// 8 - OTP token not allowed
// 9 - invalid password strength
// 10 - user successfully updated
// 11 - invalid user id
func TestUserUpsertRoute(t *testing.T) {
	initUser("UserUpsert", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description: "user successfully added",
			method:      "PUT",
			route:       "/api/user",
			requestBody: map[string]interface{}{
				"username": "CRUD" + userTest.Username,
				"password": NormalPass,
				"email":    "CRUD" + userTest.Email,
				"status":   userTest.Status,
			},
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"username":"` + `CRUD` + userTest.Username + `",`,
				`"email":"` + `CRUD` + userTest.Email + `",`,
				`"status":` + strconv.FormatBool(userTest.Status) + `,`,
				`"role":null`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description: "invalid username length",
			method:      "PUT",
			route:       "/api/user",
			requestBody: map[string]interface{}{
				"username": "abc",
				"password": NormalPass,
				"email":    "CRUD" + userTest.Email,
				"status":   userTest.Status,
			},
			expectedCode:      500,
			expectedBodyTexts: []string{`"error":"username: the length must be between 5 and 30."`},
			expectedBodyMsg:   "Error to validate User",
			token:             getUserToken(service, "NORMAL"),
		},
		{
			description: "invalid password length",
			method:      "PUT",
			route:       "/api/user",
			requestBody: map[string]interface{}{
				"username": "CRUD" + userTest.Username,
				"password": "abc",
				"email":    "CRUD" + userTest.Email,
				"status":   userTest.Status,
			},
			expectedCode:      500,
			expectedBodyTexts: []string{`"error":"password: the length must be between 8 and 20."`},
			expectedBodyMsg:   "Error to validate User password",
			token:             getUserToken(service, "NORMAL"),
		},
		{
			description: "invalid email",
			method:      "PUT",
			route:       "/api/user",
			requestBody: map[string]interface{}{
				"username": "CRUD" + userTest.Username,
				"password": NormalPass,
				"email":    "email",
				"status":   userTest.Status,
			},
			expectedCode:      500,
			expectedBodyTexts: []string{`"error":"email: must be a valid email address."`},
			expectedBodyMsg:   "Error to validate User",
			token:             getUserToken(service, "NORMAL"),
		},
		{
			description: "missing status and role",
			method:      "PUT",
			route:       "/api/user",
			requestBody: map[string]interface{}{
				"username": "CRUD2" + userTest.Username,
				"password": NormalPass,
				"email":    "CRUD2" + userTest.Email,
			},
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"username":"` + `CRUD2` + userTest.Username + `",`,
				`"email":"` + `CRUD2` + userTest.Email + `",`,
				`"status":false`},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "PUT",
			route:           "/api/user",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "PUT",
			route:           "/api/user",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "PUT",
			route:           "/api/user",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
		{
			description: "invalid password strength",
			method:      "PUT",
			route:       "/api/user",
			requestBody: map[string]interface{}{
				"username": "CRUD" + userTest.Username,
				"password": "abcabcab",
				"email":    "CRUD" + userTest.Email,
				"status":   userTest.Status,
			},
			expectedCode: 500,
			expectedBodyTexts: []string{
				`"error":""`,
				`"msg":"Error checking password strength"`,
				`the score is 1`,
				`shows in pwnedpasswords with occurence: 226`,
			},
			expectedBodyMsg: "Error checking password strength",
			token:           getUserToken(service, "NORMAL"),
		},
		{
			description: "user successfully updated",
			method:      "PUT",
			route:       "/api/user",
			requestBody: map[string]interface{}{
				"id":       userTest.ID.String(),
				"username": "CRUDUp" + userTest.Username,
				"email":    "CRUDUp" + userTest.Email,
				"status":   false,
			},
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"username":"CRUDUp` + userTest.Username + `",`,
				`"email":"CRUDUp` + userTest.Email + `",`,
				`"status":false,`,
				`"role":null`},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description: "invalid user id",
			method:      "PUT",
			route:       "/api/user",
			requestBody: map[string]interface{}{
				"id":       "12345678-1234-1234-1234-123456789012",
				"username": "CRUDInv" + userTest.Username,
				"email":    "CRUDInv" + userTest.Email,
				"status":   false,
			},
			expectedCode:    200,
			expectedBodyMsg: "Error to update User",
			expectedBodyTexts: []string{
				`"username":"` + `CRUDInv` + userTest.Username + `",`,
				`"email":"` + `CRUDInv` + userTest.Email + `",`,
				`"status":` + strconv.FormatBool(false) + `,`,
				`"role":null`,
			},
			token: getUserToken(service, "NORMAL"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, "Response body is different from expected | "+test.description)
		}

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
			App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "CRUD%").Exec(ctx)
			App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "CRUD%").ForceDelete().Exec(ctx)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestUserListRoute - Tests for user list route
// 1 - users successfully listed
// 2 - invalid token
// 3 - empty token
// 4 - OTP token not allowed
func TestUserListRoute(t *testing.T) {
	initUser("1CRUDUserList", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)
	initUser("2CRUDUserList", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)
	initUser("3CRUDUserList", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)
	initUser("UserList", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description:  "users successfully listed",
			method:       "GET",
			route:        "/api/user",
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"username":"backend",`,
				`"email":"backend@backend.io",`,
				`"username":"UserListBackendTest",`,
				`"email":"UserListBackendTest@backend.io",`,
				`"username":"1CRUD` + userTest.Username + `",`,
				`"email":"1CRUD` + userTest.Email + `",`,
				`"username":"2CRUD` + userTest.Username + `",`,
				`"email":"2CRUD` + userTest.Email + `",`,
				`"username":"3CRUD` + userTest.Username + `",`,
				`"email":"3CRUD` + userTest.Email + `",`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "GET",
			route:           "/api/user",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "GET",
			route:           "/api/user",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "GET",
			route:           "/api/user",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, test.description)
		}

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestUserGetRoute - Tests for user get route
// 1 - user successfully found
// 2 - invalid user id
// 3 - invalid token
// 4 - empty token
// 5 - OTP token not allowed
func TestUserGetRoute(t *testing.T) {
	initUser("UserGet", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description:  "user successfully found",
			method:       "GET",
			route:        "/api/user/" + userTest.ID.String(),
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"id":"` + userTest.ID.String() + `",`,
				`"username":"` + userTest.Username + `",`,
				`"email":"` + userTest.Email + `",`,
				`"status":` + strconv.FormatBool(userTest.Status) + `,`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid user id",
			method:          "GET",
			route:           "/api/user/12345678-1234-1234-1234-123456789012",
			expectedCode:    500,
			expectedBodyMsg: "Error to find User",
			expectedBodyTexts: []string{
				`"error":"sql: no rows in result set"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "GET",
			route:           "/api/user/" + userTest.ID.String(),
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "GET",
			route:           "/api/user/" + userTest.ID.String(),
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "GET",
			route:           "/api/user/" + userTest.ID.String(),
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, test.description)
		}

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestUserDeleteRoute - Tests for user delete route
// 1 - user successfully deleted
// 2 - invalid user id
// 3 - invalid token
// 4 - empty token
// 5 - OTP token not allowed
func TestUserDeleteRoute(t *testing.T) {
	initUser("UserDelete", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description:  "user successfully deleted",
			method:       "DELETE",
			route:        "/api/user/" + userTest.ID.String(),
			expectedCode: 200,
			token:        getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid user id",
			method:          "DELETE",
			route:           "/api/user/12345678-1234-1234-1234-123456789012",
			expectedCode:    500,
			expectedBodyMsg: "Error to delete User",
			expectedBodyTexts: []string{
				`"error":"record not found"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "DELETE",
			route:           "/api/user/" + userTest.ID.String(),
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "DELETE",
			route:           "/api/user/" + userTest.ID.String(),
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "DELETE",
			route:           "/api/user/" + userTest.ID.String(),
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, "Response body is different from expected | "+test.description)
		}

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestUserPassChangeRoute - Tests for user password change route
// 1 - invalid user id
// 2 - new password did not match
// 3 - invalid password length
// 4 - invalid password strength
// 5 - invalid token
// 6 - empty token
// 7 - OTP token not allowed
// 8 - user password successfully changed
func TestUserPassChangeRoute(t *testing.T) {
	initUser("UserPassChange", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	tests := []APITest{
		{
			description: "invalid user id",
			method:      "PUT",
			route:       "/api/user/password/change/12345678-1234-1234-1234-123456789012",
			requestBody: map[string]interface{}{
				"currentPassword": NormalPass,
				"password":        NormalPass,
				"passwordConfirm": NormalPass,
			},
			expectedCode:    500,
			expectedBodyMsg: "Error to change user password",
			expectedBodyTexts: []string{
				`"error":"record not found"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description: "new password did not match",
			method:      "PUT",
			route:       "/api/user/password/change/" + userTest.ID.String(),
			requestBody: map[string]interface{}{
				"currentPassword": NormalPass,
				"password":        NormalPass + "1",
				"passwordConfirm": NormalPass + "2",
			},
			expectedCode:    500,
			expectedBodyMsg: "Passwords are different",
			token:           getUserToken(service, "NORMAL"),
		},
		{
			description: "invalid password length",
			method:      "PUT",
			route:       "/api/user/password/change/" + userTest.ID.String(),
			requestBody: map[string]interface{}{
				"currentPassword": NormalPass,
				"password":        "abc",
				"passwordConfirm": "abc",
			},
			expectedCode:    500,
			expectedBodyMsg: "Error to validate new password",
			token:           getUserToken(service, "NORMAL"),
		},
		{
			description: "invalid password strength",
			method:      "PUT",
			route:       "/api/user/password/change/" + userTest.ID.String(),
			requestBody: map[string]interface{}{
				"currentPassword": NormalPass,
				"password":        "abcabcab",
				"passwordConfirm": "abcabcab",
			},
			expectedCode: 500,
			expectedBodyTexts: []string{
				`"error":""`,
				`"msg":"Error checking password strength"`,
				`the score is 1`,
				`shows in pwnedpasswords with occurence: 226`,
			},
			expectedBodyMsg: "Error checking password strength",
			token:           getUserToken(service, "NORMAL"),
		},
		{
			description: "invalid token",
			method:      "PUT",
			route:       "/api/user/password/change/" + userTest.ID.String(),
			requestBody: map[string]interface{}{
				"currentPassword": NormalPass,
				"password":        NormalPass + "1",
				"passwordConfirm": NormalPass + "1",
			},
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description: "empty token",
			method:      "PUT",
			route:       "/api/user/password/change/" + userTest.ID.String(),
			requestBody: map[string]interface{}{
				"currentPassword": NormalPass,
				"password":        NormalPass + "1",
				"passwordConfirm": NormalPass + "1",
			},
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description: "OTP token not allowed",
			method:      "PUT",
			route:       "/api/user/password/change/" + userTest.ID.String(),
			requestBody: map[string]interface{}{
				"currentPassword": NormalPass,
				"password":        NormalPass + "1",
				"passwordConfirm": NormalPass + "1",
			},
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
		{
			description: "user password successfully changed",
			method:      "PUT",
			route:       "/api/user/password/change/" + userTest.ID.String(),
			requestBody: map[string]interface{}{
				"currentPassword": NormalPass,
				"password":        NormalPass + "1",
				"passwordConfirm": NormalPass + "1",
			},
			expectedCode: 200,
			token:        getUserToken(service, "NORMAL"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, "Response body is different from expected | "+test.description)
		}

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestRoleListAllRoute - Tests for role list all route
// 1 - roles, permissions and roles permissions are successfully listed
// 2 - invalid token
// 3 - empty token
// 4 - OTP token not allowed
func TestRoleListAllRoute(t *testing.T) {
	initUser("RoleUserList", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	App.RBAC.AddRole(rbac.Role{Name: "TestListRole1"})
	App.RBAC.AddRole(rbac.Role{Name: "TestListRole2"})
	App.RBAC.AddRole(rbac.Role{Name: "TestListRole3"})

	App.RBAC.AddPermission(rbac.Permission{Model: "TestListPermissionModel1", Action: "TestListPermissionAction1"})
	App.RBAC.AddPermission(rbac.Permission{Model: "TestListPermissionModel2", Action: "TestListPermissionAction2"})
	App.RBAC.AddPermission(rbac.Permission{Model: "TestListPermissionModel3", Action: "TestListPermissionAction3"})

	App.RBAC.AddRolePermission(rbac.AddRolePermission{Role: "TestListRole1", PermissionModel: "TestListPermissionModel1", PermissionAction: "TestListPermissionAction1"})
	App.RBAC.AddRolePermission(rbac.AddRolePermission{Role: "TestListRole2", PermissionModel: "TestListPermissionModel2", PermissionAction: "TestListPermissionAction2"})
	App.RBAC.AddRolePermission(rbac.AddRolePermission{Role: "TestListRole3", PermissionModel: "TestListPermissionModel3", PermissionAction: "TestListPermissionAction3"})

	tests := []APITest{
		{
			description:  "roles, permissions and roles permissions are successfully listed",
			method:       "GET",
			route:        "/api/role/list/all",
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"roles"`,
				`"Name":"TestListRole1"`,
				`"Name":"TestListRole2"`,
				`"Name":"TestListRole3"`,
				`"permissions"`,
				`"Model":"TestListPermissionModel1"`,
				`"Model":"TestListPermissionModel2"`,
				`"Model":"TestListPermissionModel3"`,
				`"Action":"TestListPermissionAction1"`,
				`"Action":"TestListPermissionAction2"`,
				`"Action":"TestListPermissionAction3"`,
				`"rolePermissions"`,
				`"RoleID"`,
				`"Role"`,
				`"PermissionID"`,
				`"Permission"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "GET",
			route:           "/api/role/list/all",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "GET",
			route:           "/api/role/list/all",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "GET",
			route:           "/api/role/list/all",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, test.description)
		}

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.RBAC.DeleteRole("TestListRole1")
	App.RBAC.DeleteRole("TestListRole2")
	App.RBAC.DeleteRole("TestListRole3")

	App.RBAC.DeletePermission("TestListPermissionModel1", "TestListPermissionAction1")
	App.RBAC.DeletePermission("TestListPermissionModel2", "TestListPermissionAction2")
	App.RBAC.DeletePermission("TestListPermissionModel3", "TestListPermissionAction3")

	App.RBAC.DeleteRolePermission("TestListRole1", "TestListPermissionModel1", "TestListPermissionAction1")
	App.RBAC.DeleteRolePermission("TestListRole2", "TestListPermissionModel2", "TestListPermissionAction2")
	App.RBAC.DeleteRolePermission("TestListRole3", "TestListPermissionModel3", "TestListPermissionAction3")

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestRoleListRoleRoute - Tests for role list roles route
// 1 - roles are successfully listed
// 2 - invalid token
// 3 - empty token
// 4 - OTP token not allowed
func TestRoleListRoleRoute(t *testing.T) {
	initUser("RoleUserList", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	App.RBAC.AddRole(rbac.Role{Name: "TestListRole1"})
	App.RBAC.AddRole(rbac.Role{Name: "TestListRole2"})
	App.RBAC.AddRole(rbac.Role{Name: "TestListRole3"})

	tests := []APITest{
		{
			description:  "roles are successfully listed",
			method:       "GET",
			route:        "/api/role/list/roles",
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"roles"`,
				`"Name":"TestListRole1"`,
				`"Name":"TestListRole2"`,
				`"Name":"TestListRole3"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "GET",
			route:           "/api/role/list/roles",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "GET",
			route:           "/api/role/list/roles",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "GET",
			route:           "/api/role/list/roles",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, test.description)
		}

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.RBAC.DeleteRole("TestListRole1")
	App.RBAC.DeleteRole("TestListRole2")
	App.RBAC.DeleteRole("TestListRole3")

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestRoleListPermissionRoute - Tests for role list permissions route
// 1 - permissions are successfully listed
// 2 - invalid token
// 3 - empty token
// 4 - OTP token not allowed
func TestRoleListPermissionRoute(t *testing.T) {
	initUser("RoleUserList", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	App.RBAC.AddPermission(rbac.Permission{Model: "TestListPermissionModel1", Action: "TestListPermissionAction1"})
	App.RBAC.AddPermission(rbac.Permission{Model: "TestListPermissionModel2", Action: "TestListPermissionAction2"})
	App.RBAC.AddPermission(rbac.Permission{Model: "TestListPermissionModel3", Action: "TestListPermissionAction3"})

	tests := []APITest{
		{
			description:  "permissions are successfully listed",
			method:       "GET",
			route:        "/api/role/list/permissions",
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"permissions"`,
				`"Model":"TestListPermissionModel1"`,
				`"Model":"TestListPermissionModel2"`,
				`"Model":"TestListPermissionModel3"`,
				`"Action":"TestListPermissionAction1"`,
				`"Action":"TestListPermissionAction2"`,
				`"Action":"TestListPermissionAction3"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "GET",
			route:           "/api/role/list/permissions",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "GET",
			route:           "/api/role/list/permissions",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "GET",
			route:           "/api/role/list/permissions",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, test.description)
		}

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.RBAC.DeletePermission("TestListPermissionModel1", "TestListPermissionAction1")
	App.RBAC.DeletePermission("TestListPermissionModel2", "TestListPermissionAction2")
	App.RBAC.DeletePermission("TestListPermissionModel3", "TestListPermissionAction3")

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestRoleListRolesPermissionRoute - Tests for role list roles permisisons route
// 1 - roles permissions are successfully listed
// 2 - invalid token
// 3 - empty token
// 4 - OTP token not allowed
func TestRoleListRolesPermissionRoute(t *testing.T) {
	initUser("RoleUserList", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	App.RBAC.AddRole(rbac.Role{Name: "TestListRole1"})
	App.RBAC.AddRole(rbac.Role{Name: "TestListRole2"})
	App.RBAC.AddRole(rbac.Role{Name: "TestListRole3"})

	App.RBAC.AddPermission(rbac.Permission{Model: "TestListPermissionModel1", Action: "TestListPermissionAction1"})
	App.RBAC.AddPermission(rbac.Permission{Model: "TestListPermissionModel2", Action: "TestListPermissionAction2"})
	App.RBAC.AddPermission(rbac.Permission{Model: "TestListPermissionModel3", Action: "TestListPermissionAction3"})

	App.RBAC.AddRolePermission(rbac.AddRolePermission{Role: "TestListRole1", PermissionModel: "TestListPermissionModel1", PermissionAction: "TestListPermissionAction1"})
	App.RBAC.AddRolePermission(rbac.AddRolePermission{Role: "TestListRole2", PermissionModel: "TestListPermissionModel2", PermissionAction: "TestListPermissionAction2"})
	App.RBAC.AddRolePermission(rbac.AddRolePermission{Role: "TestListRole3", PermissionModel: "TestListPermissionModel3", PermissionAction: "TestListPermissionAction3"})

	tests := []APITest{
		{
			description:  "roles permissions are successfully listed",
			method:       "GET",
			route:        "/api/role/list/rolesPermissions",
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"rolePermissions"`,
				`"RoleID"`,
				`"Role"`,
				`"PermissionID"`,
				`"Permission"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "GET",
			route:           "/api/role/list/rolesPermissions",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "GET",
			route:           "/api/role/list/rolesPermissions",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "GET",
			route:           "/api/role/list/rolesPermissions",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		respBody := readBodyMsg(body)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, test.description)
		}

		if test.expectedCode == 200 {
			assert.NotContains(t, respBody.Msg, `"msg":`, "Message is different from expected | "+test.description)
		} else {
			assert.Equal(t, test.expectedBodyMsg, respBody.Msg, "Message is different from expected | "+test.description)
		}
	}

	App.RBAC.DeleteRole("TestListRole1")
	App.RBAC.DeleteRole("TestListRole2")
	App.RBAC.DeleteRole("TestListRole3")

	App.RBAC.DeletePermission("TestListPermissionModel1", "TestListPermissionAction1")
	App.RBAC.DeletePermission("TestListPermissionModel2", "TestListPermissionAction2")
	App.RBAC.DeletePermission("TestListPermissionModel3", "TestListPermissionAction3")

	App.RBAC.DeleteRolePermission("TestListRole1", "TestListPermissionModel1", "TestListPermissionAction1")
	App.RBAC.DeleteRolePermission("TestListRole2", "TestListPermissionModel2", "TestListPermissionAction2")
	App.RBAC.DeleteRolePermission("TestListRole3", "TestListPermissionModel3", "TestListPermissionAction3")

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestRoleAddRoute - Tests for role add route
// 1 - role successfully added
// 2 - duplicate role
// 3 - invalid token
// 4 - empty token
// 5 - OTP token not allowed
func TestRoleAddRoute(t *testing.T) {
	initUser("RoleAdd", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	App.RBAC.DeleteRole("RoleTest")

	tests := []APITest{
		{
			description: "role successfully added",
			method:      "POST",
			route:       "/api/role/add/role",
			requestBody: map[string]interface{}{
				"name": "RoleTest",
			},
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"Name":"` + `RoleTest"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description: "duplicate role",
			method:      "POST",
			route:       "/api/role/add/role",
			requestBody: map[string]interface{}{
				"name": "RoleTest",
			},
			expectedCode: 400,
			expectedBodyTexts: []string{
				`"msg":"Error to create Role"`,
				`"error":"Duplicate Role"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "POST",
			route:           "/api/role/add/role",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "POST",
			route:           "/api/role/add/role",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "POST",
			route:           "/api/role/add/role",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, "Response body is different from expected | "+test.description)
		}

	}

	App.DB.NewDelete().Model(&rbac.Role{}).Where("name like ?", "%RoleTest%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestPermissionAddRoute - Tests for permission add route
// 1 - permission successfully added
// 2 - duplicate permission
// 3 - invalid token
// 4 - empty token
// 5 - OTP token not allowed
func TestPermissionAddRoute(t *testing.T) {
	initUser("PermissionAdd", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	App.RBAC.DeletePermission("PermissionModelTest", "PermissionActionTest")

	tests := []APITest{
		{
			description: "permission successfully added",
			method:      "POST",
			route:       "/api/role/add/permission",
			requestBody: map[string]interface{}{
				"model":  "PermissionModelTest",
				"action": "PermissionActionTest",
			},
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"Model":"` + `PermissionModelTest"`,
				`"Action":"` + `PermissionActionTest"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description: "duplicate permission",
			method:      "POST",
			route:       "/api/role/add/permission",
			requestBody: map[string]interface{}{
				"model":  "PermissionModelTest",
				"action": "PermissionActionTest",
			},
			expectedCode: 400,
			expectedBodyTexts: []string{
				`"msg":"Error to create Permission"`,
				`"error":"Duplicate Permission"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "POST",
			route:           "/api/role/add/permission",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "POST",
			route:           "/api/role/add/permission",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "POST",
			route:           "/api/role/add/permission",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, "Response body is different from expected | "+test.description)
		}

	}

	App.DB.NewDelete().Model(&rbac.Permission{}).Where("model like ?", "%PermissionModelTest%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestRolePermissionAddRoute - Tests for role permission add route
// 1 - role permission successfully added
// 2 - duplicate role permission
// 3 - invalid token
// 4 - empty token
// 5 - OTP token not allowed
func TestRolePermissionAddRoute(t *testing.T) {
	initUser("RolePermissionAdd", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	App.RBAC.DeleteRolePermission("RoleTest", "PermissionModelTest", "PermissionActionTest")

	App.RBAC.AddRole(rbac.Role{Name: "RoleTest"})
	App.RBAC.AddPermission(rbac.Permission{Model: "PermissionModelTest", Action: "PermissionActionTest"})

	tests := []APITest{
		{
			description: "role permission successfully added",
			method:      "POST",
			route:       "/api/role/add/rolePermission",
			requestBody: map[string]interface{}{
				"role":             "RoleTest",
				"permissionModel":  "PermissionModelTest",
				"permissionAction": "PermissionActionTest",
			},
			expectedCode: 200,
			expectedBodyTexts: []string{
				`"RoleID":"`,
				`"PermissionID":"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description: "duplicate role permission",
			method:      "POST",
			route:       "/api/role/add/rolePermission",
			requestBody: map[string]interface{}{
				"role":             "RoleTest",
				"permissionModel":  "PermissionModelTest",
				"permissionAction": "PermissionActionTest",
			},
			expectedCode: 400,
			expectedBodyTexts: []string{
				`"msg":"Error to create Role Permission"`,
				`"error":"Duplicate Role Permission"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "POST",
			route:           "/api/role/add/rolePermission",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "POST",
			route:           "/api/role/add/rolePermission",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "POST",
			route:           "/api/role/add/rolePermission",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, "Response body is different from expected | "+test.description)
		}

	}

	App.DB.NewDelete().Model(&rbac.Role{}).Where("name like ?", "%RoleTest%").Exec(ctx)
	App.DB.NewDelete().Model(&rbac.Permission{}).Where("model like ?", "%PermissionModelTest%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestRoleDeleteRoute - Tests for role delete route
// 1 - role successfully deleted
// 2 - role not found
// 3 - invalid token
// 4 - empty token
// 5 - OTP token not allowed
func TestRoleDeleteRoute(t *testing.T) {
	initUser("RoleDelete", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	App.RBAC.AddRole(rbac.Role{Name: "RoleTest"})

	tests := []APITest{
		{
			description:  "role successfully deleted",
			method:       "DELETE",
			route:        "/api/role/delete/role/RoleTest",
			expectedCode: 200,
			token:        getUserToken(service, "NORMAL"),
		},
		{
			description:  "role not found",
			method:       "DELETE",
			route:        "/api/role/delete/role/RoleTest",
			expectedCode: 400,
			expectedBodyTexts: []string{
				`"msg":"Error to delete Role"`,
				`"error":"Record not found"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "DELETE",
			route:           "/api/role/delete/role/abc",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "DELETE",
			route:           "/api/role/delete/role/abc",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "DELETE",
			route:           "/api/role/delete/role/abc",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, "Response body is different from expected | "+test.description)
		}

	}

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestPermissionDeleteRoute - Tests for permission delete route
// 1 - permission successfully deleted
// 2 - permission not found
// 3 - invalid token
// 4 - empty token
// 5 - OTP token not allowed
func TestPermissionDeleteRoute(t *testing.T) {
	initUser("PermissionDelete", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	App.RBAC.AddPermission(rbac.Permission{Model: "PermissionModelTest", Action: "PermissionActionTest"})

	tests := []APITest{
		{
			description:  "permission successfully deleted",
			method:       "DELETE",
			route:        "/api/role/delete/permission/PermissionModelTest/PermissionActionTest",
			expectedCode: 200,
			token:        getUserToken(service, "NORMAL"),
		},
		{
			description:  "permission not found",
			method:       "DELETE",
			route:        "/api/role/delete/permission/PermissionModelTest/PermissionActionTest",
			expectedCode: 400,
			expectedBodyTexts: []string{
				`"msg":"Error to delete Permission"`,
				`"error":"Record not found"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "DELETE",
			route:           "/api/role/delete/permission/abc/abc",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "DELETE",
			route:           "/api/role/delete/permission/abc/abc",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "DELETE",
			route:           "/api/role/delete/permission/abc/abc",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, "Response body is different from expected | "+test.description)
		}

	}

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// TestRolePermissionDeleteRoute - Tests for role permission delete route
// 1 - role permission successfully deleted
// 2 - role permission not found
// 3 - invalid token
// 4 - empty token
// 5 - OTP token not allowed
func TestRolePermissionDeleteRoute(t *testing.T) {
	initUser("RPDelete", HashPass)
	App.DB.NewInsert().Model(&userTest).On("CONFLICT (username) DO UPDATE").Exec(ctx)

	App.RBAC.AddRole(rbac.Role{Name: "RoleTest"})
	App.RBAC.AddPermission(rbac.Permission{Model: "PermissionModelTest", Action: "PermissionActionTest"})
	App.RBAC.AddRolePermission(rbac.AddRolePermission{Role: "RoleTest", PermissionModel: "PermissionModelTest", PermissionAction: "PermissionActionTest"})

	tests := []APITest{
		{
			description:  "role permission successfully deleted",
			method:       "DELETE",
			route:        "/api/role/delete/rolePermission/RoleTest/PermissionModelTest/PermissionActionTest",
			expectedCode: 200,
			token:        getUserToken(service, "NORMAL"),
		},
		{
			description:  "role permission not found",
			method:       "DELETE",
			route:        "/api/role/delete/rolePermission/RoleTest/PermissionModelTest/PermissionActionTest",
			expectedCode: 400,
			expectedBodyTexts: []string{
				`"msg":"Error to delete Role Permission"`,
				`"error":"Record not found"`,
			},
			token: getUserToken(service, "NORMAL"),
		},
		{
			description:     "invalid token",
			method:          "DELETE",
			route:           "/api/role/delete/rolePermission/abc/abc/abc",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           "jdhryshajkietdgsbnju",
		},
		{
			description:     "empty token",
			method:          "DELETE",
			route:           "/api/role/delete/rolePermission/abc/abc/abc",
			expectedCode:    400,
			expectedBodyMsg: "Missing or malformed token",
			token:           "",
		},
		{
			description:     "OTP token not allowed",
			method:          "DELETE",
			route:           "/api/role/delete/rolePermission/abc/abc/abc",
			expectedCode:    401,
			expectedBodyMsg: "Invalid or expired token",
			token:           getUserToken(service, "OTP"),
		},
	}

	for _, test := range tests {

		res, body, err := doRequest(test)

		assert.Nilf(t, err, "Error sending request | "+test.description)
		assert.Equalf(t, test.expectedCode, res.StatusCode, "Status code is different from expected | "+test.description)

		assert.NotContains(t, string(body), `"token":`, "Token should not have been generated | "+test.description)

		for _, txt := range test.expectedBodyTexts {
			assert.Containsf(t, string(body), txt, "Response body is different from expected | "+test.description)
		}

	}

	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").Exec(ctx)
	App.DB.NewDelete().Model(&models.User{}).Where("username like ?", "%"+userTest.Username+"%").ForceDelete().Exec(ctx)
}

// getUserToken - Generates a new token for the user to be used in tests
func getUserToken(service *fiber.App, tokenType string) string {

	reqBody, _ := json.Marshal(map[string]string{
		"username": userTest.Username,
		"password": NormalPass,
	})
	req, _ := http.NewRequest("POST", "/api/auth", bytes.NewBuffer(reqBody))
	req.Header.Set("Content-Type", "application/json")

	out := "token"
	switch tokenType {
	case "NORMAL":
		_, _ = App.DB.NewUpdate().Model(&userTest).WherePK().Set("otp_status = ?", false).Set("otp_recovery_codes = ?", nil).Exec(ctx)
	case "OTP":
		req.Header.Set("Authorization", "Bearer "+getUserToken(service, "NORMAL"))
		_, _ = App.DB.NewUpdate().
			Model((*models.User)(nil)).
			Set("otp_status = ?", true).
			Set("otp_recovery_codes = ?", pgdialect.Array(RecoveryCodes)).
			Where("id = ?", userTest.ID).
			Exec(ctx)

		out = "otp"
	}

	res, _ := service.Test(req, -1)
	body, _ := ioutil.ReadAll(res.Body)

	var respBody handlers.ResponseData
	_ = json.Unmarshal(body, &respBody)

	return respBody.Data[out].(string)
}

// doRequest - Prepare and execute the requests
// Returns the response, the body in bytes and the error
func doRequest(test APITest) (*http.Response, []byte, error) {
	// Marshall request body
	reqBody, _ := json.Marshal(test.requestBody)

	// Create a new http request with the route from the test case
	req, _ := http.NewRequest(test.method, test.route, bytes.NewBuffer(reqBody))
	if len(test.requestBody) > 0 {
		req.Header.Set("Content-Type", "application/json")
	}

	// Set bearer token
	if test.token != "" {
		req.Header.Set("Authorization", "Bearer "+test.token)
	}

	// Perform the request plain with the app.
	// The -1 disables request latency.
	res, _ := service.Test(req, -1)

	// Read the response body
	body, err := ioutil.ReadAll(res.Body)

	return res, body, err
}

// readBodyMsg - Read the body and unmarshal to users.ResponseData model
func readBodyMsg(body []byte) (respBody handlers.ResponseData) {
	_ = json.Unmarshal(body, &respBody)
	return respBody
}

// initUser - Initialize user that will be used in test
func initUser(testName, password string) {
	userTest = models.User{
		Model: db.Model{
			ID: uuid.New(),
		},
		Email:    testName + "BackendTest@backend.io",
		Username: testName + "BackendTest",
		Password: password,
		Status:   true,
		Role:     []string{"admin"},
	}
}
