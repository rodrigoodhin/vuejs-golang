# API

Backend API

## Getting started

To run Backend you need golang installed in your computer

## Description
Backend API is the center of the platform, initially we will be using packages after to separate in microservices.

## Token Cache
Before start the API, you need to start a docker container with redis. Redis it's a data storage used to store token cache.

### Start
```
docker run --name redis-backend -p 36379:6379 -d redis
```

### Stop
```
docker container stop $(docker container ls -aq --filter "ancestor=olricio/olricd:latest" | awk '{print $1;}')
```

*Before start API, you need to start olric docker container*

## Usage
go run .

## Swagger

*[Local](http://localhost:7000/swagger/)*

### Install swagger docs
```
go get -u github.com/swaggo/swag/cmd/swag
```

### Update swagger docs
```
swag init --parseDependency --parseInternal
```

## Prometheus Agent
```
docker run -p 9090:9090 -v /path/to/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus
```


## Postgres
*This command must be running once when postgres were installed*
```
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```