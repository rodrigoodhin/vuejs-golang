import "bootstrap/dist/css/bootstrap.min.css";
import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";

import App from "./App.vue";

// My Components
import Home from "./components/Home";
import Login from "./components/Login";
import ForgotPassword from "./components/ForgotPassword";

// Creating the app
const app = createApp(App);

// Routes
const routes = [
  { path: "/", component: Home },
  { path: "/login", component: Login },
  { path: "/forgot-password", component: ForgotPassword },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
app.use(router);

// Add components

// Mount the app
app.mount("#app");

import "bootstrap/dist/js/bootstrap.js";
