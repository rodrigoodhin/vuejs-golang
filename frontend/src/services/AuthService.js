import axios from 'axios';
const url = 'http://localhost:7000/api/';
export default {
  login(credentials) {
    return axios
      .post(url + 'auth/', credentials)
      .then(response => response.data);
  }
};